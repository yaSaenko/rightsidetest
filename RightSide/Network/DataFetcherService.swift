//
//  DataFetcherService.swift
//  RightSide
//
//  Created by Iaroslav Saenko on 02.03.2020.
//  Copyright © 2020 Iaroslav Saenko. All rights reserved.
//

import Foundation
import UIKit

final class DataFetcherService {
    class var shared: DataFetcherService {
        struct SingletonWrapper {
            static let singleton = DataFetcherService(networkDataFetcher: NetworkDataFetcher(networking: NetworkService()))
        }
        return SingletonWrapper.singleton
    }

    var networkDataFethcer: NetworkDataFetcher
    let apiKey = "4ddac7232860daf3db38f212a0828796"

    private init(networkDataFetcher: NetworkDataFetcher) {
        self.networkDataFethcer = networkDataFetcher
    }

    func fetchCategories(completion: @escaping (CategoryList?) -> Void) {
        let urlString = "https://wall.alphacoders.com/api2.0/get.php?auth=4ddac7232860daf3db38f212a0828796&method=category_list&page=1"
        networkDataFethcer.fetchGenericJSONData(urlString: urlString, response: completion)
    }

    func fetcSubhCategories(category: Int, completion: @escaping (SubCategoryList?) -> Void) {
        let urlString = "https://wall.alphacoders.com/api2.0/get.php?auth=4ddac7232860daf3db38f212a0828796&method=sub_category_list&id=\(category)&page=1"
        networkDataFethcer.fetchGenericJSONData(urlString: urlString, response: completion)
    }

    func fetcSubCategory(category: Int, completion: @escaping (WallpaperList?) -> Void) {
        let urlString = "https://wall.alphacoders.com/api2.0/get.php?auth=4ddac7232860daf3db38f212a0828796&method=sub_category&id=\(category)&page=1"
        networkDataFethcer.fetchGenericJSONData(urlString: urlString, response: completion)
    }

    func fetchWallpapers(url: String, completion: @escaping (WallpaperList?) -> Void) {
        let urlString = url
        networkDataFethcer.fetchGenericJSONData(urlString: urlString, response: completion)
    }

    func fetchImage(url: String, completion: @escaping (UIImage?) -> Void) {
        let urlString = url
        networkDataFethcer.fetchImage(urlString: urlString, response: completion)
    }
}
