//
//  NetworkDataFetcher.swift
//  RightSide
//
//  Created by Iaroslav Saenko on 02.03.2020.
//  Copyright © 2020 Iaroslav Saenko. All rights reserved.
//

import Foundation
import UIKit

protocol DataFetcher {
    func fetchGenericJSONData<T: Decodable>(urlString: String, response: @escaping (T?) -> Void)
}

class NetworkDataFetcher {
    var networking: Networking

    init(networking: Networking) {
        self.networking = networking
    }

    func fetchGenericJSONData<T: Decodable>(urlString: String, response: @escaping (T?) -> Void) {
        networking.request(urlString: urlString) { (data, error) in
            if error != nil {
                fatalError("Couldn't get data")
            }
            let decoded = self.decodeJSON(type: T.self, from: data)
            response(decoded)
        }
    }

    func decodeJSON<T: Decodable>(type: T.Type, from: Data?) -> T? {
        let decoder = JSONDecoder()
        guard let data = from else { return nil }
        do {
            let objects = try decoder.decode(type, from: data)
            return objects
        } catch {
            fatalError("Couldn't decode")
        }
    }

    func fetchImage(urlString: String, response: @escaping (UIImage?) -> Void) {
        networking.request(urlString: urlString) { data, error in
            if error != nil {
                fatalError("Couldn't get data")
            }
            guard let data = data, let image = UIImage(data: data) else { return }
            response(image)
        }
    }
}
