//
//  Category.swift
//  RightSide
//
//  Created by Iaroslav Saenko on 02.03.2020.
//  Copyright © 2020 Iaroslav Saenko. All rights reserved.
//

import Foundation

struct Category: Codable {
    let id: Int
    let name: String
    let count: Int
    let url: String
}

struct CategoryList: Codable {
    let categories: [Category]?
}
