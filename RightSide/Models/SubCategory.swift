//
//  SubCategory.swift
//  RightSide
//
//  Created by Iaroslav Saenko on 03.03.2020.
//  Copyright © 2020 Iaroslav Saenko. All rights reserved.
//

import Foundation

struct SubCategory: Codable {
    let id: Int
    let name: String
    let url: String
}

struct SubCategoryList: Codable {
    let subCategories: [SubCategory]

    private enum CodingKeys: String, CodingKey {
        case subCategories = "sub-categories"
    }
}
