//
//  Wallpaper.swift
//  RightSide
//
//  Created by Iaroslav Saenko on 04.03.2020.
//  Copyright © 2020 Iaroslav Saenko. All rights reserved.
//

import Foundation

struct Wallpaper: Codable {
    let id: String
    let width: String
    let height: String
    let urlImage: String

    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case urlImage = "url_image"
        case width = "width"
        case height = "height"
    }
}

struct WallpaperList: Codable {
    let wallpapers: [Wallpaper]
}
