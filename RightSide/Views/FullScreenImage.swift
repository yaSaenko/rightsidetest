//
//  FullScreenImage.swift
//  RightSide
//
//  Created by Iaroslav Saenko on 04.03.2020.
//  Copyright © 2020 Iaroslav Saenko. All rights reserved.
//

import Foundation
import UIKit

class FullScreenImage: UIImageView {
    // For Gesture
    var fullFit: Bool = false {
        didSet {
            if fullFit {
                UIView.animate(withDuration: 0.4) { [weak self] in
                    self?.transform = CGAffineTransform(scaleX: 3, y: 3)
                }
            } else {
                UIView.animate(withDuration: 0.4) { [weak self] in
                    self?.transform = CGAffineTransform(scaleX: 1, y: 1)
                }
            }
        }
    }

    init(image: UIImage) {
        super.init(image: image)
        isUserInteractionEnabled = true
        contentMode = .scaleAspectFit
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showFull(sender:)))
        tapGestureRecognizer.numberOfTapsRequired = 2
        addGestureRecognizer(tapGestureRecognizer)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: Gestures
extension FullScreenImage {
    @objc func showFull(sender: UITapGestureRecognizer) {
        switch sender.state {
        case .ended:
            fullFit = !fullFit
        default:
            ()
        }
    }
}
