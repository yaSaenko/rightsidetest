//
//  WallpaperCollectionViewCell.swift
//  RightSide
//
//  Created by Iaroslav Saenko on 04.03.2020.
//  Copyright © 2020 Iaroslav Saenko. All rights reserved.
//

import UIKit

class WallpaperCollectionViewCell: UICollectionViewCell {
    static let reuseId = "GalleryCell"
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    // MARK: Services
    weak var delegate: WallpaperViewDelegate!
    let network = DataFetcherService.shared

    // MARK: Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        image.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        image.isUserInteractionEnabled = true
        spinner.isHidden = false
        spinner.startAnimating()

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showFull(sender:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.image.addGestureRecognizer(tapGestureRecognizer)
        self.image.contentMode = .scaleToFill
    }

    func setImage(url: String) {
        network.fetchImage(url: url) { image in
            guard let image = image else { return }
            self.image.image = image
            self.spinner.stopAnimating()
            self.spinner.isHidden = true
        }
    }

}

// MARK: Gestures
extension WallpaperCollectionViewCell {
    @objc private func showFull(sender: UITapGestureRecognizer) {
        switch sender.state {
        case .ended:
            guard let image = image.image else { return }
            delegate.showImageFullScreen(image: image)
        default:
            ()
        }
    }
}

// MARK: DelegateProtocol
protocol WallpaperViewDelegate: class {
    func showImageFullScreen(image: UIImage)
}
