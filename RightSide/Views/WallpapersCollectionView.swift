//
//  WallpapersCollectionView.swift
//  RightSide
//
//  Created by Iaroslav Saenko on 04.03.2020.
//  Copyright © 2020 Iaroslav Saenko. All rights reserved.
//

import UIKit

class WallpapersCollectionView: UICollectionView {
    // MARK: Services
    let network = DataFetcherService.shared
    weak var del: WallpaperViewDelegate!

    // MARK: Models
    var wallpapers: [Wallpaper] = []

    init(frame: CGRect) {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: frame.width/3.2, height: frame.height/5.2)
        layout.estimatedItemSize = CGSize(width: frame.width/3.2, height: frame.height/5.2)

        super.init(frame: frame, collectionViewLayout: layout)

        delegate = self
        dataSource = self

        backgroundColor = #colorLiteral(red: 0.5928955674, green: 0.6355044842, blue: 0.6830866933, alpha: 1)

        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false

        register(UINib(nibName: "WallpaperCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: WallpaperCollectionViewCell.reuseId)

    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: Delegation methods
extension WallpapersCollectionView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return wallpapers.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dequeueReusableCell(withReuseIdentifier: WallpaperCollectionViewCell.reuseId, for: indexPath) as! WallpaperCollectionViewCell
        let wallpaper = wallpapers[indexPath.row]
        cell.delegate = del
        cell.setImage(url: wallpaper.urlImage)
        return cell
    }
}
