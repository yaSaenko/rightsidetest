//
//  AppDelegate.swift
//  RightSide
//
//  Created by Iaroslav Saenko on 02.03.2020.
//  Copyright © 2020 Iaroslav Saenko. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        let mainVC = MainViewController()
        let rootNavigationController = UINavigationController(rootViewController: mainVC)
        window?.rootViewController = rootNavigationController
        window?.makeKeyAndVisible()
        return true
    }
}
