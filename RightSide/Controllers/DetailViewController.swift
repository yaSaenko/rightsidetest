//
//  DetailViewController.swift
//  RightSide
//
//  Created by Iaroslav Saenko on 03.03.2020.
//  Copyright © 2020 Iaroslav Saenko. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    // MARK: Services
    let network = DataFetcherService.shared

    // MARK: Models
    var category = 0
    var subCategories: [SubCategory] = []

    // MARK: UIElements
    private var table: UITableView!
    private var refreshControl: UIRefreshControl = {
        let refrechControl = UIRefreshControl()
        refrechControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refrechControl.attributedTitle = NSAttributedString(string: "Загружаем данные")
        return refrechControl
    }()

    // MARK: VC LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
    }

    private func setupTable() {
        table = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height), style: .plain)
        self.view.addSubview(table)
        table.delegate = self
        table.dataSource = self

        table.separatorStyle = .singleLine
        table.backgroundColor = .darkGray

        table.refreshControl = refreshControl
        self.refresh()
    }

    @objc private func refresh() {
        self.refreshControl.beginRefreshing()
        network.fetcSubhCategories(category: category) { (data) in
            if let data = data?.subCategories {
                DispatchQueue.main.async {
                    self.subCategories = data
                    self.refreshControl.endRefreshing()
                    self.table.reloadData()
                }
            } else {
                self.alert(message: "Не удалось загрузить данные", title: "Ошибка сети")
                self.refreshControl.endRefreshing()
            }
        }
    }
}

// MARK: Delegation Methods
extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCategories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        let category = subCategories[indexPath.row]
        cell.textLabel?.text = category.name
        cell.detailTextLabel?.text = category.name
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let wvc = WallpapersViewController()
        wvc.subCategoryId = subCategories[indexPath.row].id
        wvc.title = subCategories[indexPath.row].name
        self.navigationController?.pushViewController(wvc, animated: true)
    }
}
