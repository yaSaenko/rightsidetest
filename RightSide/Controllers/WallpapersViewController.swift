//
//  WallpapersCollectionViewController.swift
//  RightSide
//
//  Created by Iaroslav Saenko on 03.03.2020.
//  Copyright © 2020 Iaroslav Saenko. All rights reserved.
//

import UIKit

class WallpapersViewController: UIViewController {
    // MARK: Services
    let network = DataFetcherService.shared

    // MARK: Models
    var subCategoryId: Int!

    // MARK: VC LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        network.fetcSubCategory(category: subCategoryId) { data in
            if let data = data?.wallpapers {
                DispatchQueue.main.async {
                    let colview = WallpapersCollectionView(frame: self.view.frame)
                    colview.wallpapers = data
                    colview.del = self
                    self.view.addSubview(colview)
                }
            }
        }
    }
}

// MARK: WallpaperViewDelegate
extension WallpapersViewController: WallpaperViewDelegate {
    func showImageFullScreen(image: UIImage) {
        let tempVC = UIViewController()
        let iview = FullScreenImage(image: image)
        tempVC.view = iview
        self.navigationController?.pushViewController(tempVC, animated: false)
    }
}
