//
//  ViewController.swift
//  RightSide
//
//  Created by Iaroslav Saenko on 02.03.2020.
//  Copyright © 2020 Iaroslav Saenko. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    // MARK: Services
    let network = DataFetcherService.shared

    // MARK: Models
    var categories: [Category] = []

    // MARK: UIElements
    private var table: UITableView!

    private var refreshControl: UIRefreshControl = {
        let refrechControl = UIRefreshControl()
        refrechControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refrechControl.attributedTitle = NSAttributedString(string: "Загружаем данные")
        return refrechControl
    }()

    // MARK: VC LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Get wallpaper!"
        setupTable()
    }

    private func setupTable() {
        table = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height), style: .plain)
        self.view.addSubview(table)
        table.delegate = self
        table.dataSource = self

        table.separatorStyle = .singleLine
        table.backgroundColor = .darkGray

        table.refreshControl = refreshControl
        self.refresh()
    }

    @objc private func refresh() {
        self.refreshControl.beginRefreshing()
        network.fetchCategories { (data) in
            if let data = data?.categories {
                DispatchQueue.main.async {
                    self.categories = data
                    self.refreshControl.endRefreshing()
                    self.table.reloadData()
                }
            } else {
                self.alert(message: "Не удалось загрузить данные", title: "Ошибка сети")
            }
        }
    }
}

// MARK: Delegation Methods
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        let category = categories[indexPath.row]
        cell.detailTextLabel?.text = category.name
        cell.textLabel?.text = category.name
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dvc = DetailViewController()
        dvc.category = categories[indexPath.row].id
        dvc.title = categories[indexPath.row].name
        self.navigationController?.pushViewController(dvc, animated: true)
    }
}
